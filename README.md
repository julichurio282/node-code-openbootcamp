
# openbootcamp MERN course

### by Julian Churio

>**Dependencies I've installed**
> - *Express*: This will help us create our backend application with Node.js
> - *dotenv*: Dotenv will help us load environment variables from a .env file into process.env

>**Dev Dependencies**
> - *Concurrently*: This will allow us to run multiple commands at the same time from our package.json
> - *Eslint*: Eslint is used to keep a consistent coding style within the project. We are using the standard Eslint configuration
> - *Jest*: A testing framework
> - *Nodemon*: A tool that helps develop Node.js based applications by automatically restarting the node application when file changes in the directory are detected
> - *serve*: serve helps you serve a static site, single page application or just a static file
> - *Webpack*: Its main purpose is to bundle JavaScript files for usage in a browser