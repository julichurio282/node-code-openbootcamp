import express, { Express, Request, Response } from 'express'
import dotenv from 'dotenv';

// Configure the .env file

dotenv.config();

// Create Express APP

const app: Express = express();

const port: string | number = process.env.PORT || 8000;

// Define the first Route of the app

app.get("/", (req: Request, res: Response) => {
    // Send hello world
    res.send("Welcome to my API Restful: Express + TS + Nodemon + Jest + Swagger + Mongoose");
});

app.get("/hello", (req: Request, res: Response) => {
    // Send hello world
    const data = {
        'message' : `Hello ${req.query.name ? req.query.name : 'anonimo'}!`
    }
    res.json(data.message);
    console.log(req.query)
});

const USERS = [
    {
        name: 'juli',
        age: 23
    },
    {
        name: 'eichen',
        age: 25
    }
]

app.get('/users', (req: Request, res: Response) => {
    console.log(res.statusCode)
    const newa = USERS.map((a) => a.name)
    res.json(newa)
})




// Execute APP and Listen Requests to PORT

app.listen(port, () => console.log(`EXPRESS SERVER: Running at http://localhost:${port}`));
